package com.dcits.dto;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.dcits.business.server.ViewServerInfo;
import com.dcits.business.userconfig.UserSpace;
import java.util.*;

/**
 * 后台监控详情
 * @author xuwangcheng14@163.com
 * @version 1.0.0
 * @description
 * @date 2022/6/2  20:25
 */
public class SpaceBackgroundDetailsDTO {

    /**
     * 后台记录时间间隔
     */
    private Integer monitoringIntervalTime;
    /**
     * 开启后台记录
     */
    private Boolean backgroundRecord;
    /**
     * 结束时间
     */
    private Date recordEndTime;
    /**
     * 当前服务器数量
     */
    private Integer serverCount;
    /**
     * 正在后台监控的服务器数量
     */
    private Integer monitoringServerCount;
    /**
     * 已经产生的数据量
     */
    private Integer monitoringInfoCount;
    /**
     * 监控数据量大小
     */
    private String monitoringInfoSize;
    /**
     * 所有服务器信息
     */
    private Map<String, List<SimpleServerInfo>> servers = new HashMap<>();

    public SpaceBackgroundDetailsDTO (UserSpace userSpace) {
        this.backgroundRecord = userSpace.isBackgroundRecord();
        if (this.backgroundRecord) {
            this.recordEndTime = userSpace.getRecordEndTime();
            this.monitoringIntervalTime = userSpace.getMonitoringIntervalTime();
            this.monitoringServerCount = userSpace.getRecordInfoData().getInteger("serverCount");
            this.monitoringInfoCount = userSpace.getRecordInfoData().getInteger("infoCount");
            this.monitoringInfoSize = FileUtil.readableFileSize((long) StrUtil.length(userSpace.getRecordInfoData().toJSONString()));
        }

        this.serverCount = userSpace.getServerCount();
        for (String serverType:userSpace.getServers().keySet()) {
            List<SimpleServerInfo> serverInfos = servers.get(serverType);
            if (serverInfos == null) {
                serverInfos = new ArrayList<>();
                servers.put(serverType, serverInfos);
            }

            if (CollUtil.isNotEmpty(userSpace.getServers().get(serverType))) {
                for (ViewServerInfo viewServerInfo:userSpace.getServers().get(serverType)) {
                    serverInfos.add(new SimpleServerInfo(viewServerInfo));
                }
            }
        }
    }

    public Integer getMonitoringIntervalTime() {
        return monitoringIntervalTime;
    }

    public void setMonitoringIntervalTime(Integer monitoringIntervalTime) {
        this.monitoringIntervalTime = monitoringIntervalTime;
    }

    public Boolean getBackgroundRecord() {
        return backgroundRecord;
    }

    public void setBackgroundRecord(Boolean backgroundRecord) {
        this.backgroundRecord = backgroundRecord;
    }

    public Date getRecordEndTime() {
        return recordEndTime;
    }

    public void setRecordEndTime(Date recordEndTime) {
        this.recordEndTime = recordEndTime;
    }

    public Integer getServerCount() {
        return serverCount;
    }

    public void setServerCount(Integer serverCount) {
        this.serverCount = serverCount;
    }

    public Integer getMonitoringServerCount() {
        return monitoringServerCount;
    }

    public void setMonitoringServerCount(Integer monitoringServerCount) {
        this.monitoringServerCount = monitoringServerCount;
    }

    public Integer getMonitoringInfoCount() {
        return monitoringInfoCount;
    }

    public void setMonitoringInfoCount(Integer monitoringInfoCount) {
        this.monitoringInfoCount = monitoringInfoCount;
    }

    public String getMonitoringInfoSize() {
        return monitoringInfoSize;
    }

    public void setMonitoringInfoSize(String monitoringInfoSize) {
        this.monitoringInfoSize = monitoringInfoSize;
    }

    public Map<String, List<SimpleServerInfo>> getServers() {
        return servers;
    }

    public void setServers(Map<String, List<SimpleServerInfo>> servers) {
        this.servers = servers;
    }

    public static class SimpleServerInfo {
        private Integer viewId;
        private String host;
        private String tags;
        private Boolean backgroundRecord;
        private Integer count = 0;
        private Date startTime;
        private Date lastTime;


        public SimpleServerInfo (ViewServerInfo viewServerInfo) {
            this.host = StrUtil.blankToDefault(viewServerInfo.getRealHost(), viewServerInfo.getHost()) + ":" + viewServerInfo.getPort();
            this.tags = viewServerInfo.getTags();
            this.backgroundRecord = viewServerInfo.isBackgroundRecord();
            this.count = viewServerInfo.getCount();
            this.startTime = viewServerInfo.getStartTime();
            this.lastTime = viewServerInfo.getLastTime();
            this.viewId = viewServerInfo.getViewId();
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getLastTime() {
            return lastTime;
        }

        public void setLastTime(Date lastTime) {
            this.lastTime = lastTime;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getTags() {
            return tags;
        }

        public void setTags(String tags) {
            this.tags = tags;
        }

        public Boolean getBackgroundRecord() {
            return backgroundRecord;
        }

        public void setBackgroundRecord(Boolean backgroundRecord) {
            this.backgroundRecord = backgroundRecord;
        }

        public Integer getViewId() {
            return viewId;
        }

        public void setViewId(Integer viewId) {
            this.viewId = viewId;
        }
    }

}
