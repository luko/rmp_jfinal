package com.dcits.tool.ssh;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import com.dcits.business.server.linux.LinuxServer;
import com.dcits.business.server.websocker.WebSocketController;
import com.dcits.dto.ResponseSocketDTO;
import com.jcraft.jsch.*;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class SSHUtil {

    private static final Logger logger = Logger.getLogger(SSHUtil.class);

    private static final Set<String> EXEC_COMMAND_TAGS = new HashSet<String>();

    /**
     * 获取ssh连接
     *
     * @param host
     * @param port
     * @param username
     * @param password
     * @return
     * @throws IOException
     */
    public static Session getConnection(String host, int port, String username, String password) throws JSchException {
        JSch jsch = new JSch();
        Session session = null;

        try {
            session = jsch.getSession(username, host, port);
            session.setPassword(password);

            Properties props = new Properties();
            props.put("StrictHostKeyChecking", "no");
            session.setConfig(props);
            session.connect();
        } catch (JSchException e) {
            logger.error("开启session失败:\n" + e.getMessage());
            throw e;
        }
        return session;
    }


    /**
     * 发送停止标记
     *
     * @param tag
     */
    public static void stopExecCommand(String tag) {
        EXEC_COMMAND_TAGS.add(tag);
    }


    /**
     * 执行一次命令
     * <br>返回结果
     * <br>命令一定要是一次性执行完毕的命令(非交互性命令)
     *
     * @param session    ssh连接对象
     * @param command 执行命令
     * @return
     * @throws Exception
     * @throws IOException
     */
    public static String execCommand(Session session, String command, int count, int getMode, String tag) throws Exception {
        if (session == null || !session.isConnected()) {
            return null;
        }

        logger.debug(StrUtil.format("Execute command {} to {}", command, session.getHost()));
        Channel channel = null;
        try {
            channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            channel.setInputStream(null);
            InputStream output = channel.getInputStream();

            channel.connect();
            String result = IoUtil.read(new InputStreamReader(output), true);
            // 去除可能存在的\n
            if (result.endsWith("\n") || result.endsWith("\t")) {
                result = result.substring(0, result.length() - 1);
            }
            logger.debug(StrUtil.format("Result of command {} on {}: {}", command, session.getHost(), result));

            return result;
        } catch (JSchException | IOException e) {
            logger.error(StrUtil.format("Failed to execute command {} on {} due to {}", command, session.getHost(), e.toString()));
            return null;
        } finally {
            if (channel != null && !channel.isClosed()) {
                channel.disconnect();
            }
        }
    }

    /**
     *
     * 交互命令执行
     *
     * @param channel    ssh连接对象
     * @param command 执行命令
     * @param count   读取多少行,从第一行开始读取
     * @param getMode 返回模式，0-只返回正确的输出 1-只返回错误的信息 2-返回所有的信息<br>mode=2时将会使用终端模式
     * @param tag     标记,防止中断命令时在并发情况下出现混乱
     * @return
     * @throws Exception
     * @throws IOException
     */
    public static String execCommandShell(Channel channel, String command, int count, int getMode, String tag){
        return null;
    }


    public static boolean channelExec(LinuxServer server, String command, javax.websocket.Session socketSession, ResponseSocketDTO socketDTO, String tag) throws JSchException {
        Channel channel = null;
        try {
            channel = server.getJschSession().openChannel("shell");
        } catch (JSchException e) {
            server.getJschSession().disconnect();
            server.connectssh();
            channel = server.getJschSession().openChannel("shell");
        }

        //通道连接 超时时间3s
        channel.connect(3000);

//            //设置channel
        server.setChannel(channel);

        //转发消息
        InputStream inputStream = null;
        try {
            transToSSH(channel, command);
            inputStream = channel.getInputStream();
            //循环读取
            byte[] buffer = new byte[1024];
            int i = 0;
            //如果没有数据来，线程会一直阻塞在这个地方等待数据。
            while ((i = inputStream.read(buffer)) != -1) {
                String readLine = new String(buffer, 0, i);
                logger.debug(readLine);
                socketDTO.setCommandResponse(readLine);
                WebSocketController.PutMessageToQueue(socketSession, socketDTO, tag);
                if (WebSocketController.FLAG) {
                    WebSocketController.sendQueueMsg();
                    WebSocketController.FLAG = false;
                }
                if (EXEC_COMMAND_TAGS.contains(tag)) {
                    channel.disconnect();
                    server.setChannel(null);
                    break;
                }

            }
            return true;

        }catch (Exception e){
            channel.disconnect();
            server.setChannel(null);
            logger.error(e);
        }  finally {
            //断开连接后关闭会话
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.error(e);
                }
            }
        }

        return false;

    }

    /**
     * @Description: 将消息转发到终端
     * @Param: [channel, data]
     * @return: void
     * @Author: NoCortY
     * @Date: 2020/3/7
     */
    public static void transToSSH(Channel channel, String command) throws IOException {
        if (channel != null) {
            OutputStream outputStream = channel.getOutputStream();
            outputStream.write(command.getBytes());
            outputStream.flush();
        }
    }


}
