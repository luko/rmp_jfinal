package com.dcits.mvc.common.controller;

import com.dcits.business.userconfig.UserSpace;
import com.dcits.constant.ConstantReturnCode;
import com.dcits.mvc.base.BaseController;
import com.dcits.mvc.common.model.CommandConfig;
import com.dcits.mvc.common.model.ServerInfo;
import com.dcits.mvc.common.service.CommandConfigService;
import com.dcits.tool.StringUtils;
import org.apache.log4j.Logger;

import java.util.List;


public class CommandConfigController extends BaseController {

//    private static final Logger logger = Logger.getLogger(CommandConfigController.class);
    private static CommandConfigService commandService = new CommandConfigService();

    public void listByPage() {
        String briefCommand = getPara("briefCommand");
        String useFlag = getPara("useFlag");
		int limit = Integer.valueOf(getPara("limit"));
		int page = Integer.valueOf(getPara("page"));
        List<CommandConfig> resultList = commandService.listByPage(briefCommand,
                UserSpace.getUserSpace(getPara("userKey")).getUserConfig().getId(), useFlag,
                limit, page);
        List<CommandConfig> resultAll = commandService.listAll(briefCommand,
                UserSpace.getUserSpace(getPara("userKey")).getUserConfig().getId(), useFlag);
        renderSuccess(resultList, "", resultAll.size());
    }

    public void listAll() {
        String briefCommand = getPara("briefCommand");
        List<CommandConfig> resultAll = commandService.listAll(briefCommand,
                UserSpace.getUserSpace(getPara("userKey")).getUserConfig().getId(), "1");
        renderSuccess(resultAll,"");
    }

    public void edit() {
        CommandConfig commandConfig = getModel(CommandConfig.class, "", true);
        String briefCommand = getPara("briefCommand");
        String useFlagType = getPara("useFlagType");
        int configId = UserSpace.getUserSpace(getPara("userKey")).getUserConfig().getId();
        commandConfig.setConfigId(configId);
        if ( StringUtils.isEmpty(getPara("useFlagType")) && StringUtils.isEmpty(getPara("command"))){
            renderError(ConstantReturnCode.VALIDATE_FAIL, "指令不能为空！！！");
            return;
        }
        if (StringUtils.isNotEmpty(briefCommand)){
            List<CommandConfig> list = commandService.findCommandByBriefCommand(briefCommand.trim(),"",configId);
            if (list != null &&  list.size() > 0 ){
                renderError(ConstantReturnCode.VALIDATE_FAIL, "快捷方式冲突！！！");
                return;
            }
        }

        commandService.edit(commandConfig);
        //更新正在监控的服务器基本信息
        renderSuccess(commandConfig, "");
    }

    public void del() {
        commandService.deleteById(getParaToInt("id"));
        renderSuccess(null, "删除成功");
    }

}
