package com.dcits.mvc.common.service;

import com.dcits.constant.CustomConfig;
import com.dcits.mvc.common.model.ServerInfo;
import com.dcits.tool.StringUtils;

import java.util.Date;
import java.util.List;

public class ServerInfoService {
	
	private static final ServerInfo dao = new ServerInfo().dao();
	
	public boolean edit(ServerInfo serverInfo) {
		if (serverInfo.getId() == null) {
			serverInfo.setCreateTime(new Date());
			return serverInfo.save();
		} else {
			return serverInfo.update();
		}
	}
	
	public ServerInfo checkRepeat(ServerInfo serverInfo) {
		String sql = "select * from " + ServerInfo.TABLE_NAME + " where " + ServerInfo.column_host + "="
				+ "? and " + ServerInfo.column_port + "=? and " + ServerInfo.column_username + "=?";
		if (!CustomConfig.SHARE_SERVER_LIST) {
		    sql += " and " + ServerInfo.column_config_id + "=" + serverInfo.getConfigId();
        }
		if (serverInfo.getId() != null) {
			sql += " and " + ServerInfo.column_id + "<>?";
			return dao.findFirst(sql, serverInfo.getHost(), serverInfo.getPort(), serverInfo.getUsername(), serverInfo.getId());
		} else {
			return dao.findFirst(sql, serverInfo.getHost(), serverInfo.getPort(), serverInfo.getUsername());
		}
	}
	
	public List<ServerInfo> listAll(String serverType, int configId) {
		String sql = "select * from " + ServerInfo.TABLE_NAME + " where 1=1";
        if (!CustomConfig.SHARE_SERVER_LIST) {
            sql +=  " and " + ServerInfo.column_config_id + "=" + configId;
        }
		if (StringUtils.isNotEmpty(serverType)) {
			sql += " and serverType='" + serverType + "'";
		}
		return dao.find(sql);
	}
	public List<ServerInfo> listByPage(String serverType, int configId,int limit,int page) {
		String sql = "select * from " + ServerInfo.TABLE_NAME + " where 1=1";
        if (!CustomConfig.SHARE_SERVER_LIST) {
            sql += " and " + ServerInfo.column_config_id + "=" + configId;
        }
		if (StringUtils.isNotEmpty(serverType)) {
			sql += " and serverType='" + serverType + "'";
		}
		sql += " limit "+(page-1)*limit+","+page*limit+" ";
		return dao.find(sql);
	}

	public ServerInfo findById(int id) {
		return dao.findById(id);
	}
	
	public void deleteById(int id) {
		dao.deleteById(id);
	}
	
	public void batchDelete(String ids) {
		for (String id:ids.split(",")) {
			deleteById(Integer.parseInt(id));
		}
	}
	
	public void updateLastTime(ServerInfo info) {
		info.set(ServerInfo.column_last_usetime, new Date()).update();
	}
}
