package com.dcits.business.server.linux;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.annotation.JSONField;
import com.dcits.business.server.MonitoringInfo;
import com.dcits.business.server.ViewServerInfo;
import com.dcits.business.server.linux.constant.CommandConstant;
import com.dcits.business.server.linux.constant.UserCustomLinuxCommand;
import com.dcits.business.server.linux.parse.ParseInfo;
import com.dcits.constant.ConstantGlobalAttributeName;
import com.dcits.mvc.common.model.UserConfig;
import com.dcits.tool.SessionKit;
import com.dcits.tool.StringUtils;
import com.dcits.tool.ssh.SSHUtil;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class LinuxServer extends ViewServerInfo {

	/**
	 * 
	 */

	protected static final Logger logger = Logger.getLogger(LinuxServer.class);
	
	public static final String SERVER_TYPE_NAME = "linux";

    protected static final long serialVersionUID = 1L;
    protected static final Map<String, Object> alertThreshold = new HashMap<String, Object>();
	public static final String LINUX_TYPE = "Linux";
	public static final String HPUX_TYPE = "HP-UX";
	public static final String SUNOS_TYPE = "SunOS";
	
	static {
		alertThreshold.put(LinuxMonitoringInfo.CPU_FREE, 90);
		alertThreshold.put(LinuxMonitoringInfo.MEMORY_FREE, 90);
		alertThreshold.put(LinuxMonitoringInfo.DISK_USER, 90);
		alertThreshold.put(LinuxMonitoringInfo.IO_WAIT, 20);
		alertThreshold.put(LinuxMonitoringInfo.TCP_CLOSE_WAIT, 20000);
		alertThreshold.put(LinuxMonitoringInfo.TCP_TIME_WAIT, 20000);
	}
	
	/****************************************************/
    protected String uname = LINUX_TYPE;
    protected Integer cpuInfo = 0;
    protected Long memInfo = 0L;
    protected String[] mountDevices = new String[0];
	/**
	 * 可使用的命令
	 */
	@JSONField(serialize=false)
    protected Map<String, String> commandMap;

    protected String networkCardName;
	/****************************************************/
    @JSONField(serialize=false)
    protected Session jschSession;
    @JSONField(serialize=false)
    protected Channel channel;

	public LinuxServer() {

		super(new LinuxMonitoringInfo());
	}
	
	public LinuxServer(MonitoringInfo monitoringInfo) {

		super(monitoringInfo);
	}
	
	@Override
	public String connect() {

		try {
			this.jschSession = SSHUtil.getConnection(getHost(), getPort(), getUsername(), getPassword());
		} catch (Exception e) {
			return "连接失败:" + e.getMessage();
		}
		if (this.jschSession != null) {
			//获取基本信息
			try {
				this.setUname(SSHUtil.execCommand(jschSession, CommandConstant.LINUX_COMMAND_MAP.get(CommandConstant.GET_UNAME), 1, 0, ""));
                UserConfig userConfig = (UserConfig) SessionKit.get().getAttribute(ConstantGlobalAttributeName.LOGIN_USER);
                UserCustomLinuxCommand customLinuxCommand = userConfig.parseLinuxCommandSetting();

				switch (this.getUname()) {
				case LINUX_TYPE:
					this.setCommandMap(customLinuxCommand.getLinuxCommand());
					break;
				case HPUX_TYPE:
					this.setCommandMap(customLinuxCommand.getHpCommand());
					break;
				case SUNOS_TYPE:
					this.setCommandMap(customLinuxCommand.getSunCommand());
					break;
				default:
					this.setCommandMap(customLinuxCommand.getLinuxCommand());
					break;
				}
				
				this.setCpuInfo(Integer.valueOf(SSHUtil.execCommand(jschSession, this.getCommandMap().get(CommandConstant.GET_CPU_INFO), 1, 0, "")));
				this.setMemInfo(Long.valueOf(SSHUtil.execCommand(jschSession, this.getCommandMap().get(CommandConstant.GET_MEMORY_INFO), 1, 0, "")));
				this.setMountDevices(SSHUtil.execCommand(jschSession, this.getCommandMap().get(CommandConstant.GET_MOUNT_DEVICE_INFO), 999999, 0, "").split("\\n"));
			} catch (Exception e) {
                logger.error(e);
			}
					
		}		
		return "true";
	}



	@Override
	public boolean disconect() {
	    try {
            if (this.channel != null) {
                this.channel.disconnect();
            }
            if (this.jschSession != null) {
                this.jschSession.disconnect();

            }
            this.jschSession = null;
            return true;
        } catch (Exception e) {
	        logger.error(e.getMessage());
	        return false;
        }

	}

	@Override
	public void getMonitoringInfo() {

		if (this.jschSession == null) {
			this.connectStatus = "无法连接主机";
			return;
		}
		monitoringInfo();
	}

	/**
	 * 使用jps命令检查当前java进程
	 * @return
	 * @throws Exception 
	 */
	public String checkJps(String javaHome) throws Exception {
		if (this.jschSession == null) {
			throw new Exception("请先连接到主机!");
		}
		return SSHUtil.execCommand(this.jschSession, javaHome + "/bin/jps|grep -vi jps", 100, 0, "");
	}
	
	/**
	 * 如果登录用户是root,则可以自行获取javaHome
	 * @return
	 */
	public String parseJavaHome() {
		String javaHome = "";
		try {
			javaHome = SSHUtil.execCommand(this.jschSession, "find / -name jps|sed -n '1p'", 1, 0, "");
			if (StringUtils.isNotEmpty(javaHome)) {
				javaHome = javaHome.substring(0, javaHome.lastIndexOf("/")).replace("/bin", "");
			}
		} catch (Exception e) {
            logger.error(e);
		}
		return javaHome;
	}
	
	public void setMountDevices(String[] mountDevices) {
		this.mountDevices = mountDevices;
	}
	public String[] getMountDevices() {
		return mountDevices;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public Integer getCpuInfo() {
		return cpuInfo;
	}

	public void setCpuInfo(Integer cpuInfo) {
		this.cpuInfo = cpuInfo;
	}

	public Long getMemInfo() {
		return memInfo;
	}

	public void setMemInfo(Long memInfo) {
		this.memInfo = memInfo;
	}
	
	public void setCommandMap(Map<String, String> commandMap) {
		this.commandMap = commandMap;
	}
	
	public Map<String, String> getCommandMap() {
		return commandMap;
	}

	public Session getJschSession() {
		return jschSession;
	}

	public void setJschSession(Session jschSession) {
		this.jschSession = jschSession;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	/**
	 * 获取要监控的网卡的正则表达式
	 * @author xuwangcheng
	 * @date 2020/3/12 20:22
	 * @param
	 * @return {@link String}
	 */
	protected String getNetWordCardName() {
	    if (StringUtils.isEmpty(this.networkCardName)) {
            LinuxExtraParameter parameter = null;
            try {
                parameter = JSONObject.parseObject(this.getParameters(), new TypeReference<LinuxExtraParameter>(){});
            } catch (Exception e) {
            }

            if (parameter != null && StringUtils.isNotEmpty(parameter.getNetworkCardName())) {
                this.networkCardName = parameter.getNetworkCardName().replace(",", "|") + "|IFACE";
            } else {
                this.networkCardName = "eth0|eno1|IFACE";
            }
        }
	    return this.networkCardName;
    }
	
	/**
	 * 获取动态信息
	 */
	public void monitoringInfo (){
		//实例化不同类型主机的信息解析类实例
		ParseInfo parseUtil = ParseInfo.getParseUtilInstance(this.uname);	
		
		LinuxMonitoringInfo monitoringInfo = (LinuxMonitoringInfo) this.info;
        this.connectStatus = "true";
        String flag;
		try {
			//cpu  内存信息
            flag = parseUtil.parseVmstatInfo(SSHUtil.execCommand(this.jschSession, this.commandMap.get(CommandConstant.VMSTAT), 5, 0, "")
					, monitoringInfo, this.uname, this.memInfo);				
			if (!"true".equals(flag)) {
                this.connectStatus = flag;
            }

			//处理tcp端口
			flag = parseUtil.parseTcpInfo(SSHUtil.execCommand(this.jschSession
					, this.commandMap.get(CommandConstant.GET_TCP_PORT_COUNT), 1, 0, ""), monitoringInfo);
            if (!"true".equals(flag)) {
                this.connectStatus = flag;
            }

			//处理网络带宽
			flag  = parseUtil.parseNetworkInfo(SSHUtil.execCommand(this.jschSession
					, this.commandMap.get(CommandConstant.GET_NETWORK_INFO).replace("NETWORKCARDNAME", getNetWordCardName()), 100, 0, "")
					, monitoringInfo);
            if (!"true".equals(flag)) {
                this.connectStatus = flag;
            }
			
			//处理磁盘空间使用信息 匹配 /和/username挂载的磁盘
			//while ((str = diskBrStat.readLine()).isEmpty()) {}	
			flag = parseUtil.parseDiskInfo(SSHUtil.execCommand(this.jschSession
					, this.commandMap.get(CommandConstant.GET_DISK_INFO), 1, 0, ""), monitoringInfo);
            if (!"true".equals(flag)) {
                this.connectStatus = flag;
            }

			//处理设备IO读写
			flag = parseUtil.parseDeviceIOInfo(SSHUtil.execCommand(this.jschSession, this.commandMap.get(CommandConstant.GET_IO_INFO)
					.replace("DEVICECOUNT", String.valueOf(this.mountDevices.length + 6)), 9999, 0, ""), monitoringInfo);
            if (!"true".equals(flag)) {
                this.connectStatus = flag;
            }

			monitoringInfo.setTime(new Date());
		} catch (Exception e) {
            logger.error("获取linux信息发生错误", e);
			this.connectStatus = "获取信息发生错误:" + e.getMessage();
			this.disconect();
		}
		
	}

	public String connectssh() {
		try {
			this.jschSession = SSHUtil.getConnection(getHost(),getPort(),getUsername(),getPassword());
		} catch (JSchException e) {
			return "连接失败:" + e.getMessage();
		}
		if (jschSession != null){
		 	return "true";
		}
		return "连接失败:";

	}

	public boolean disJschSession(){
		try {
			if (this.channel != null) {
				this.channel.disconnect();
			}
			if (this.jschSession != null ){
				this.jschSession.disconnect();
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
}
