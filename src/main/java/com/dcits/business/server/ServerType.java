package com.dcits.business.server;

import com.dcits.tool.StringUtils;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * 获取类型信息
 * @author xuwangcheng
 * @version 1.0.0.0,2018.328
 *
 */
public class ServerType {
	
	public static final String KEY_PREFIX = "server.type";
	
	//类型基本类
	public static final String TYPE_CLASS_SUFFIX = "Server";
	//监控信息类
	public static final String MONITORING_CLASS_SUFFIX = "MonitoringInfo";
	//附加参数类
	public static final String PARAMETER_CALSS_SUFFIX = "ExtraParameter";
	//controller类
	public static final String SERVER_CONTROLLER_CLASS_SUFFIX = "ServerController";
	
	public static final Map<String, Class<?>> typeClasses = new HashMap<String, Class<?>>();
	public static final Map<String, Class<?>> monitoringClasses = new HashMap<String, Class<?>>();
	public static final Map<String, Class<?>> parameterClasses = new HashMap<String, Class<?>>();
	public static final Map<String, Class<?>> controllerClasses = new HashMap<String, Class<?>>();
	
	private static final Logger logger = Logger.getLogger(ServerType.class);
	
	public static void setAllServerType() {
		Prop p = PropKit.use("server.properties");
		String[] types = p.get(KEY_PREFIX).split(",");
		for (String typeName:types) {
			String packageName = p.get(KEY_PREFIX + "." + typeName);
			String typeNameUpp = StringUtils.toUpperCaseFirstOne(typeName);
			try {
				typeClasses.put(typeName, Class.forName(packageName + "." + typeNameUpp + TYPE_CLASS_SUFFIX));
				monitoringClasses.put(typeName, Class.forName(packageName + "." + typeNameUpp + MONITORING_CLASS_SUFFIX));
				parameterClasses.put(typeName, Class.forName(packageName + "." + typeNameUpp + PARAMETER_CALSS_SUFFIX));
				controllerClasses.put(typeName, Class.forName(packageName + "." + typeNameUpp + SERVER_CONTROLLER_CLASS_SUFFIX));
			} catch (Exception e) {
                logger.error(e);
				logger.info(typeName + "类型的对应实现类不存在或者配置错误,请检查!");
			}			
		}
	}
}
