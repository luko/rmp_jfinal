package com.dcits.business.server;

import cn.hutool.core.thread.ThreadUtil;
import com.alibaba.fastjson.annotation.JSONField;
import com.dcits.business.userconfig.UserSpace;
import com.dcits.mvc.common.model.ServerInfo;
import com.dcits.tool.ThreadPoolUtil;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;

/**
 * 视图信息通用类，用于给前端数据展示用
 * @author xuwangcheng
 * @version 1.0.0.0, 2018.3.28
 *
 */
public abstract class ViewServerInfo extends ServerInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static Integer ID_COUNT = 0; 
	
	protected Integer viewId;
	
	protected MonitoringInfo info;
	
	protected String connectStatus;
	
	/**
	 * 记录命令执行或者信息获取失败的信息
	 */
	@JSONField(serialize = false)
	protected String commandExecErrorMsg;
	/**
	 * 开启后台记录
	 */
	@JSONField(serialize = false)
	protected boolean backgroundRecord;
	/**
	 * 后台记录结束时间：默认1天，最小10分钟，最大10天
	 */
	@JSONField(serialize = false)
	protected Date recordEndTime;
	/**
	 * 监控开始时间
	 */
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	protected Date startTime;
	/**
	 * 监控结束时间
	 */
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	protected Date lastTime;
	/**
	 * 监控条目数量
	 */
	protected int count;
	/**
	 * userKey
	 */
	@JSONField(serialize = false)
	protected String userKey;


	public ViewServerInfo() {
		super();
		this.viewId = ID_COUNT++;

	}
	
	public ViewServerInfo(MonitoringInfo info) {
		this();
		this.info = info;
	}
	
	/********************************************************/
	/**
	 * 连接监控服务器
	 * @return flag = true 为正常 其他为异常信息
	 */
	public abstract String connect();
	/**
	 * 断开连接
	 * @return
	 */
	public abstract boolean disconect();
	/**
	 * 获取监控信息
	 */
	public abstract void getMonitoringInfo();

	/**
	 * 后台监控
	 */
	public void backgroundMonitoringInfo(){
		if (!this.backgroundRecord || new Date().compareTo(this.recordEndTime) >= 0) {
			return;
		}
		ThreadPoolUtil.execThread(new Runnable() {
			@Override
			public void run() {
				getMonitoringInfo();
			}
		});
		if (this.connectStatus.equals("true")) {
			this.count++;
			// 如果开启了后台记录，则将结果保存到列表中
			this.lastTime = this.info.time;
			UserSpace.getUserSpace(userKey).addRecordDataInfo(this.getServerType(), this.viewId, this.info);
		}
	}

	/********************************************************/
	public void setBaseServerInfo(ServerInfo serverInfo) {

		this.setHost(serverInfo.getHost());
		this.setRealHost(serverInfo.getRealHost());
		this.setPort(serverInfo.getPort());
		this.setUsername(serverInfo.getUsername());
		this.setServerType(serverInfo.getServerType());
		this.setTags(serverInfo.getTags());
		this.setPassword(serverInfo.getPassword());
		this.setParameters(serverInfo.getParameters());
		this.setId(serverInfo.getId());
	}

	public boolean isBackgroundRecord() {
		return backgroundRecord;
	}

	public void setBackgroundRecord(boolean backgroundRecord) {
		this.backgroundRecord = backgroundRecord;
	}

	public Date getRecordEndTime() {
		return recordEndTime;
	}

	public void setRecordEndTime(Date recordEndTime) {
		this.recordEndTime = recordEndTime;
	}

	public void setConnectStatus(String connectStatus) {
		this.connectStatus = connectStatus;
	}
	
	public void setCommandExecErrorMsg(String commandExecErrorMsg) {
		this.commandExecErrorMsg = commandExecErrorMsg;
	}
	
	public String getCommandExecErrorMsg() {
		return commandExecErrorMsg;
	}
	
	public String getConnectStatus() {
		return connectStatus;
	}
	
	public Integer getViewId() {
		return viewId;
	}

	public void setViewId(Integer viewId) {
		this.viewId = viewId;
	}

	public MonitoringInfo getInfo() {
		return info;
	}

	public void setInfo(MonitoringInfo info) {
		this.info = info;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public void setLastTime(Date lastTime) {
		this.lastTime = lastTime;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getLastTime() {
		return lastTime;
	}

	public int getCount() {
		return count;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;

		if (o == null || getClass() != o.getClass()) return false;

		ViewServerInfo that = (ViewServerInfo) o;

		return new EqualsBuilder()
				.appendSuper(super.equals(o))
				.append(viewId, that.viewId)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37)
				.appendSuper(super.hashCode())
				.append(viewId)
				.toHashCode();
	}
}
