package com.dcits.business.server.dubbo;

public class DubboExtraParameter {
	private String javaHome;
	private String linuxLoginUsername;
	private String linuxLoginPassword;
	private String startScriptPath;
	
	public DubboExtraParameter(String javaHome, String linuxLoginUsername, String linuxLoginPassword,
			String startScriptPath) {
		super();
		this.javaHome = javaHome;
		this.linuxLoginUsername = linuxLoginUsername;
		this.linuxLoginPassword = linuxLoginPassword;
		this.startScriptPath = startScriptPath;
	}
	
	public DubboExtraParameter() {
		super();

	}
	public String getJavaHome() {
		return javaHome;
	}
	public void setJavaHome(String javaHome) {
		this.javaHome = javaHome;
	}
	public String getLinuxLoginUsername() {
		return linuxLoginUsername;
	}
	public void setLinuxLoginUsername(String linuxLoginUsername) {
		this.linuxLoginUsername = linuxLoginUsername;
	}
	public String getLinuxLoginPassword() {
		return linuxLoginPassword;
	}
	public void setLinuxLoginPassword(String linuxLoginPassword) {
		this.linuxLoginPassword = linuxLoginPassword;
	}
	public String getStartScriptPath() {
		return startScriptPath;
	}
	public void setStartScriptPath(String startScriptPath) {
		this.startScriptPath = startScriptPath;
	}
}
