package com.dcits.business.userconfig;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.cron.CronUtil;
import cn.hutool.cron.task.Task;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dcits.business.server.MonitoringInfo;
import com.dcits.business.server.ServerType;
import com.dcits.business.server.ViewServerInfo;
import com.dcits.constant.CustomConfig;
import com.dcits.mvc.common.model.FileInfo;
import com.dcits.mvc.common.model.ServerInfo;
import com.dcits.mvc.common.model.UserConfig;
import com.dcits.mvc.common.service.FileInfoService;
import com.dcits.tool.StringUtils;
import org.apache.log4j.Logger;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

public class UserSpace {
	private static final Logger logger = Logger.getLogger(UserSpace.class);
	private static FileInfoService fileInfoService = new FileInfoService();
    private static final Map<String, UserSpace> spaces = new HashMap<>();

	private UserConfig userConfig;
	private Map<String, List<ViewServerInfo>> servers = new HashMap<String, List<ViewServerInfo>>();
	private List<ViewServerInfo> linuxServers = new ArrayList<>();
	private Integer serverCount;


	/**
	 * 开启后台记录的时候 数据，在时间到了的时候，或者提前手动终止的时候，保存到文件
	 */
	private JSONObject recordInfoData;
	/**
	 * 后台监控停止时间，到达该时间之后，停止后台保存数据，存储数据到文件
	 */
	private Date recordEndTime;

	/**
	 * 开启后台记录
	 */
	private boolean backgroundRecord;
	private String checkCronId;
	private String monitoringCronId;
	/**
	 * 后台记录时间间隔
	 */
	private Integer monitoringIntervalTime = 6;

	private static Map<String, String> DEFAULT_INFO_DATA_PROPERTY_JSON = new HashMap<>();

	static {
		String redisJson = "{commonInfo:{clientCount:[],usedMemory:[],usedMemoryMax:[],usedCpuSys:[],usedCpuUser:[],usedCpu:[],totalPercentSecondCurrent:[],keyspaceHits:[],keyspaceMisses:[],keyspaceHitRateCurrent:[],expiredKeys:[],evictedKeys:[],dbKeyCount:[]}}";
		String tomcatJson = "{commonInfo:{sessionMaxActiveCount:[],sessionActiveCount:[],sessionCounter:[],threadMaxCount:[],threadCurrentCount:[],threadCurrentBusyCount:[]}}";
		String jvmJson = "{commonInfo:{survivorSpacePercent_0:[],survivorSpacePercent_1:[],edenSpacePercent:[],oldSpacePercent:[],permSpacePercent:[],youngGCTotalCount:[],youngGCTime:[],fullGCTotalCount:[],fullGCTime:[],gcTotalTime:[],blockedThreadCount:[]}}";
		String linuxJson = "{commonInfo:{freeCpu:[],freeMem:[],ioWait:[]},tcpInfo:{ESTABLISHED:[],LISTEN:[],CLOSE_WAIT:[],TIME_WAIT:[]},networkInfo:{rx:[],tx:[]},diskInfo:{rootDisk:[],userDisk:[]},ioInfo:{ioRead:[],ioWrite:[]}}";
		String weblogicJson = "{commonInfo:{health:[],runStatus:[]},jvmInfo:{currentSize:[],freeSize:[],freePercent:[]},queueInfo:{maxThreadCount:[],pendingCount:[],idleCount:[],throughput:[],hoggingThreadCount:[]},jdbcInfo:{jdbcState:{},activeConnectionsCurrentCount:{},availableConnectionCount:{},waitingForConnectionCurrentCount:{},activeConnectionsHighCount:{}}}";
		String dubboJson = "{commonInfo:{threadMaxCount:[],threadCoreCount:[],threadLargestCount:[],threadActiveCount:[],threadTaskCount:[],memoryMax:[],memoryTotal:[],memoryUsed:[],memoryFree:[],memoryUsedPercent:[]}}";

		DEFAULT_INFO_DATA_PROPERTY_JSON.put("redis", redisJson);
		DEFAULT_INFO_DATA_PROPERTY_JSON.put("tomcat", tomcatJson);
		DEFAULT_INFO_DATA_PROPERTY_JSON.put("jvm", jvmJson);
		DEFAULT_INFO_DATA_PROPERTY_JSON.put("linux", linuxJson);
		DEFAULT_INFO_DATA_PROPERTY_JSON.put("weblogic", weblogicJson);
		DEFAULT_INFO_DATA_PROPERTY_JSON.put("dubbo", dubboJson);

		CronUtil.setMatchSecond(true);
		CronUtil.start();
	}

	private final Object lock = new Object();
	
	public UserSpace() {
		super();
		for (String typeName:ServerType.typeClasses.keySet()) {
			servers.put(typeName, new ArrayList<ViewServerInfo>());
		}		
	}
	public UserSpace(UserConfig userConfig) {
		this();
		this.userConfig = userConfig;
	}
	
	/***************************公共方法**********************************************/
	/**
	 * 查询指定用户空间有没有初始化
	 * @param userKey
	 * @return
	 */
	public static UserSpace getUserSpace(String userKey) {
		Map<String, UserSpace> spaces = getSpaces();
		return spaces.get(userKey);		
	}
	
	
	/**
	 * 获取指定用户空间
	 * @param config
	 * @return
	 */
	public static UserSpace getUserSpace(UserConfig config) {
		Map<String, UserSpace> spaces = getSpaces();
		UserSpace space = spaces.get(config.getUserKey());
		return space;
	}
	
	public static void addUserSpace(UserConfig config) {
		getSpaces().put(config.getUserKey(), new UserSpace(config));	
	}
	
	@SuppressWarnings("unchecked")
	public static Map<String, UserSpace> getSpaces() {
		return spaces;
	}
	
	/*****************************公共方法********************************************/
	/**
	 *  开启后台监控
	 * @author xuwangcheng
	 * @date 2022/6/1 17:34
	 */
	public void openBackgroundRecord(List<String> viewIds, Date endTime, int intervalTime){
		if (new Date().compareTo(endTime) >= 0) {
			throw new RuntimeException("结束时间不能小于当前时间");
		}
		if (viewIds == null || viewIds.size() == 0) {
			throw new RuntimeException("要开启后台监控的服务器不能为空");
		}
		if (intervalTime < 3) {
			intervalTime = 3;
		}
		this.monitoringIntervalTime = intervalTime;
		this.backgroundRecord = true;
		this.recordEndTime = endTime;
		initRecordInfoData();
		for (List<ViewServerInfo> infos:this.servers.values()) {
			infos.forEach(info -> {
				if (viewIds.contains(info.getViewId().toString())) {
					addRecordViewInfo(info);
					info.setBackgroundRecord(true);
					info.setRecordEndTime(endTime);
					if (info.getStartTime() == null) {
						info.setStartTime(new Date());
						info.setLastTime(new Date());
					}
				} else {
					info.setBackgroundRecord(false);
					info.setRecordEndTime(null);
				}
			});
		}
		// 开启定时任务，定时后台获取数据内容
		if (StrUtil.isBlank(this.monitoringCronId) || this.monitoringIntervalTime != intervalTime) {
			CronUtil.remove(this.monitoringCronId);
			this.monitoringCronId = CronUtil.schedule(StrUtil.format("*/{} * * * * *", intervalTime), new Task() {
				@Override
				public void execute() {
					for (List<ViewServerInfo> infos:servers.values()) {
						infos.forEach(info -> {
							info.backgroundMonitoringInfo();
						});
					}
				}
			});
		}

		if (StrUtil.isBlank(this.checkCronId)) {
			// 开启定时任务，监控是否到时间了
			this.checkCronId = CronUtil.schedule("*/30 * * * * *", new Task() {
				@Override
				public void execute() {
					// 超过设定的时间或者数据内容超过100MB时自动结束监控
					if (new Date().compareTo(recordEndTime) >= 0 || StrUtil.length(recordInfoData.toJSONString()) >= 1024 * 1024 * 100) {
						closeBackgroundRecord();
					}
				}
			});
		}
	}

	/**
	 * 添加监控信息
	 * @param serverType
	 * @param viewId
	 * @param info
	 */
	public void addRecordDataInfo (String serverType, Integer viewId, MonitoringInfo info) {
		JSONObject json = (JSONObject) JSON.toJSON(info);
		JSONObject allInfo = this.recordInfoData.getJSONObject("infoData").getJSONObject(serverType).getJSONObject(viewId.toString());

		// itemName jdbcInfo
		json.forEach((itemName, value) -> {
			if (itemName.equals("time")) {
				allInfo.getJSONArray("time").add(value);
			}

			if (value instanceof JSONObject) {
			    // itemName2 WCRMA2R
				((JSONObject)value).forEach((itemName2, value2) -> {
				    if ("null".equals(itemName2)) {
                        return;
                    }
					if (value2 instanceof JSONObject) {
					    // itemName3 activeConnectionsCurrentCount
						((JSONObject)value2).forEach((itemName3, value3) -> {
							JSONArray lastArr = allInfo.getJSONObject(itemName).getJSONObject(itemName3).getJSONArray(itemName2);
							if (lastArr == null) {
								lastArr = new JSONArray();
								allInfo.getJSONObject(itemName).getJSONObject(itemName3).put(itemName2, lastArr);
							}
							lastArr.add(value3);
						});
					} else {
						if (allInfo.getJSONObject(itemName).getJSONArray(itemName2) != null) {
							allInfo.getJSONObject(itemName).getJSONArray(itemName2).add(value2);
						}
					}
				});
			} else {
				if (allInfo.getJSONObject("commonInfo").getJSONArray(itemName) != null) {
					allInfo.getJSONObject("commonInfo").getJSONArray(itemName).add(value);
				}
			}
		});

		synchronized (lock) {
			this.recordInfoData.put("infoCount", this.recordInfoData.getIntValue("infoCount") + 1);
		}
	}

	private void initRecordInfoData(){
		if (this.recordInfoData == null) {
			this.recordInfoData = new JSONObject();
			this.recordInfoData.put("serverList", new JSONObject());
			this.recordInfoData.put("infoData", new JSONObject());
			this.recordInfoData.put("serverCount", 0);
			this.recordInfoData.put("infoCount", 0);
			this.recordInfoData.put("mark", "保存后台记录数据");
		}
	}

	private void addRecordViewInfo(ViewServerInfo viewServerInfo){
		JSONArray jsonArray = this.recordInfoData.getJSONObject("serverList").getJSONArray(viewServerInfo.getServerType());
		if (jsonArray == null) {
			jsonArray = new JSONArray();
			this.recordInfoData.getJSONObject("serverList").put(viewServerInfo.getServerType(), jsonArray);
		}

		if (!jsonArray.contains(viewServerInfo)) {
			jsonArray.add(viewServerInfo);
			this.recordInfoData.put("serverCount", this.recordInfoData.getIntValue("serverCount") + 1);
		}

		JSONObject typeInfoData = this.recordInfoData.getJSONObject("infoData").getJSONObject(viewServerInfo.getServerType());
		if (typeInfoData == null) {
			typeInfoData = new JSONObject();
			this.recordInfoData.getJSONObject("infoData").put(viewServerInfo.getServerType(), typeInfoData);
		}

		if (!typeInfoData.containsKey(String.valueOf(viewServerInfo.getViewId()))) {
			JSONObject data = new JSONObject();
			data.put("tags", viewServerInfo.getTags());
			data.put("host", (StringUtils.isNotEmpty(viewServerInfo.getRealHost()) ? viewServerInfo.getRealHost() : viewServerInfo.getHost()) + ":" + viewServerInfo.getPort());
			data.put("time", new JSONArray());
			data.putAll(JSONObject.parseObject(DEFAULT_INFO_DATA_PROPERTY_JSON.get(viewServerInfo.getServerType())));
			typeInfoData.put(String.valueOf(viewServerInfo.getViewId()), data);
		}
	}


	/**
	 *  关闭后台监控
	 * @author xuwangcheng
	 * @date 2022/6/1 17:34
	 */
	public void closeBackgroundRecord(){
		this.backgroundRecord = false;
		this.recordEndTime = null;;
		if (StrUtil.isNotBlank(this.checkCronId)) {
			CronUtil.remove(this.checkCronId);
			this.checkCronId = null;
		}
		if (StrUtil.isNotBlank(this.monitoringCronId)) {
			CronUtil.remove(this.monitoringCronId);
			this.monitoringCronId = null;
		}
		// 保存文件
		saveRecordInfoData(this.recordInfoData);
		this.recordInfoData = null;

		for (List<ViewServerInfo> infos:this.servers.values()) {
			infos.forEach(info -> {
				info.setBackgroundRecord(false);
				info.setRecordEndTime(null);
				info.setStartTime(null);
				info.setLastTime(null);
				info.setCount(0);
			});
		}
	}

	/**
	 * 保存文件
	 */
	public String saveRecordInfoData(JSONObject allData){
		BufferedOutputStream buff = null;

		try {
			JSONObject infoData = new JSONObject();
			JSONObject serverList = new JSONObject();

			JSONArray serverInfos = new JSONArray();
			JSONObject infoDataM = allData.getJSONObject("infoData");
			infoData.put("infoData", infoDataM);
			infoData.put("serverList", serverList);

			for (Map.Entry<String, Object> entry:allData.getJSONObject("serverList").entrySet()) {
				JSONArray typeArr = new JSONArray();
				serverList.put(entry.getKey(), typeArr);

				for (Object o:(JSONArray)entry.getValue()) {
					JSONObject info = (JSONObject) JSONObject.toJSON(o);
					JSONObject serverInfo = (JSONObject) JSONObject.toJSON(getServerInfo(entry.getKey(), info.getInteger("viewId")));
					if (serverInfo == null) continue;
					serverInfo.put("startTime", info.get("startTime"));
					serverInfo.put("lastTime", info.get("lastTime"));
					serverInfo.put("count", info.get("count"));
					serverInfos.add(serverInfo);
					typeArr.add(serverInfo);
				}

			}
			//保存到文件
			String rootPath = CustomConfig.DATA_FILE_SAVE_PATH;
			String fileName = System.currentTimeMillis() + "_" + userConfig.getUserKey() + ".json";
			String filePath = rootPath + fileName;

			File saveFile = new File(filePath);

			buff = new BufferedOutputStream(new FileOutputStream(saveFile));
			buff.write(infoData.toJSONString().getBytes("UTF-8"));
			buff.flush();


			FileInfo fileInfo = new FileInfo();
			fileInfo.setConfigId(userConfig.getId());
			fileInfo.setCreateTime(new Date());
			fileInfo.setFileName(fileName);
			fileInfo.setFilePath(filePath);
			fileInfo.setInfoCount(allData.getInteger("infoCount"));
			fileInfo.setServerCount(allData.getInteger("serverCount"));
			fileInfo.setServerInfos(serverInfos.toJSONString());
			fileInfo.setMark(allData.getString("mark"));
            fileInfo.setFileSize(FileUtil.readableFileSize(saveFile));

//			String fileSize = "";
//			if (saveFile.length() > 1024 * 1024) {
//				fileSize = RmpUtil.byteToMB(saveFile.length()) + "MB";
//			} else {
//				fileSize = RmpUtil.byteToKB(saveFile.length()) + "KB";
//			}


			fileInfoService.save(fileInfo);

			return filePath;
		} catch (Exception e) {
			logger.error("保存数据时失败!", e);
			throw new RuntimeException(e);
		} finally {
			if (buff != null) {
				try {
					buff.close();
				} catch (IOException e) {
					logger.error("IOException", e);
				}
			}
		}
	}

	/**
	 * 获取本用户空间下的指定类型指定viewId的正在监控的服务器信息
	 * @param typeName
	 * @param viewId
	 * @return
	 */
	public ViewServerInfo getServerInfo(String typeName, Integer viewId) {
		List<ViewServerInfo> ss = this.servers.get(typeName);
		if (ss != null) {
			for (ViewServerInfo info:ss) {
				if (viewId.equals(info.getViewId())) {
					return info;
				}
			}
		}
		return null;
	}

	/**
	 * 获取本用户空间下的指定类型指定viewId的正在监控的服务器信息
	 * @param typeName
	 * @param host
	 * @return
	 */
	public ViewServerInfo getServerInfo(String typeName, String host) {
		List<ViewServerInfo> ss = this.servers.get(typeName);
		if (ss != null) {
			for (ViewServerInfo info:ss) {
				if (host.equals(info.getHost())) {
					return info;
				}
			}
		}
		return null;
	}
	/**
	 * 获取本用户空间下的指定类型指定viewId的正在监控的服务器信息
	 * @param viewId
	 * @return
	 */
	public ViewServerInfo getServerInfo(Integer viewId) {
		List<ViewServerInfo> ss = this.linuxServers;
		if (ss != null && ss.size() > 0) {
			for (ViewServerInfo info:ss) {
				if (viewId.equals(info.getViewId())) {
					return info;
				}
			}
		}
		return null;
	}
	
	/**
	 * 根据指定的typeName和viewId删除正在监控列表的服务器信息
	 * @param typeName
	 * @param viewId
	 */
	public void delServerInfo(String typeName, Integer viewId) {
		ViewServerInfo info = getServerInfo(typeName, viewId);
		if (info != null) this.servers.get(typeName).remove(info);
		
	}
	
	public List<ServerInfo> getServerInfoById (Integer id) {
		List<ServerInfo> infos = new ArrayList<ServerInfo>();
		for (List<ViewServerInfo> list:this.servers.values()) {
			for (ViewServerInfo info:list) {
				if (id.equals(info.getId())) {
					infos.add(info);
				}
			}
		}
		return infos;
	}
	
	/**
	 * 实时更新基本信息
	 * @param id
	 * @param serverInfo
	 */
	public void updateBaseInfo (Integer id, ServerInfo serverInfo) {
		if (id == null){			
			return;
		}
		List<ServerInfo> infos = getServerInfoById(id);
		for (ServerInfo info:infos) {
			info.setHost(serverInfo.getHost());
			info.setRealHost(serverInfo.getRealHost());
			info.setPort(serverInfo.getPort());
			info.setUsername(serverInfo.getUsername());
			info.setPassword(serverInfo.getPassword());
			info.setTags(serverInfo.getTags());
			info.setParameters(serverInfo.getParameters());	
		}
	}
	
	public UserConfig getUserConfig() {
		return userConfig;
	}
	public void setUserConfig(UserConfig userConfig) {
		this.userConfig = userConfig;
	}
	
	public Map<String, List<ViewServerInfo>> getServers() {
		return servers;
	}
	public void setServers(Map<String, List<ViewServerInfo>> servers) {
		this.servers = servers;
	}

	public List<ViewServerInfo> getLinuxServers() {
		return linuxServers;
	}

	public void setLinuxServers(List<ViewServerInfo> linuxServers) {
		this.linuxServers = linuxServers;
	}

	public Integer getServerCount() {
		this.serverCount = 0;
		for (String typeName:servers.keySet()) {
			serverCount += (servers.get(typeName)).size();
		}
		return serverCount;
	}
	public void setServerCount(Integer serverCount) {
		this.serverCount = serverCount;
	}
	@Override
	public String toString() {
		return "UserSpace [userConfig=" + userConfig + ", servers=" + servers + ", serverCount=" + serverCount + "]";
	}


	public JSONObject getRecordInfoData() {
		return recordInfoData;
	}

	public void setRecordInfoData(JSONObject recordInfoData) {
		this.recordInfoData = recordInfoData;
	}

	public Date getRecordEndTime() {
		return recordEndTime;
	}

	public void setRecordEndTime(Date recordEndTime) {
		this.recordEndTime = recordEndTime;
	}

	public boolean isBackgroundRecord() {
		return backgroundRecord;
	}

	public void setBackgroundRecord(boolean backgroundRecord) {
		this.backgroundRecord = backgroundRecord;
	}

	public Integer getMonitoringIntervalTime() {
		return monitoringIntervalTime;
	}

	public static void main(String[] args) {
		String json = "{commonInfo:{clientCount:[],usedMemory:[],usedMemoryMax:[],usedCpuSys:[],usedCpuUser:[],usedCpu:[],totalPercentSecondCurrent:[],keyspaceHits:[],keyspaceMisses:[],keyspaceHitRateCurrent:[],expiredKeys:[],evictedKeys:[],dbKeyCount:[]}}";
		JSONObject obj = JSONObject.parseObject(json);
		System.out.println(obj);
	}
}
